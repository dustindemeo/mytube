#include "tablistmodel.h"

TabListModel::TabListModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

int TabListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return rowList.size();
}

int TabListModel::columnCount(const QModelIndex &/*parent*/) const
{
    return N_COLUMNS;
}

QVariant TabListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role == Qt::TextAlignmentRole) {
        if (index.column() < 4)
            return int(Qt::AlignLeft | Qt::AlignTop);
        else
            return int(Qt::AlignRight | Qt::AlignTop);
    }
    else if (role == Qt::DisplayRole) {
        switch (index.column()) {
        case 0:
            return rowList[index.row()]->pixmap;
            break;
        case 1:
            return rowList[index.row()]->credit;
            break;
        case 2:
            return rowList[index.row()]->title;
            break;
        case 3:
            return rowList[index.row()]->description;
            break;
        case 4:
            return QTime((((rowList[index.row()]->duration)/60)/60)%60, ((rowList[index.row()]->duration)/60)%60, (rowList[index.row()]->duration)%60, 0).toString("HH:mm:ss");
            break;
        case 5:
            return rowList[index.row()]->average;
            break;
        }
    }
    return QVariant();
}

QVariant TabListModel::headerData(int section, Qt::Orientation orientation,
                                int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return "Thumbnail";
            break;
        case 1:
            return "Credit";
            break;
        case 2:
            return "Title";
            break;
        case 3:
            return "Description";
            break;
        case 4:
            return "Duration";
            break;
        case 5:
            return "Average";
            break;
        }
    }
    return QVariant();
}

Qt::ItemFlags TabListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    if (index.column() == 0)
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    else
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

bool TabListModel::setData(const QModelIndex &index,
                            const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    switch (index.column()) {
    case 1:
        rowList[index.row()]->credit = value.toString();
        break;
    case 2:
        rowList[index.row()]->title = value.toString();
        break;
    case 3:
        rowList[index.row()]->description = value.toString();
        break;
    case 4:
        rowList[index.row()]->duration = value.toInt();
        break;
    case 5:
        rowList[index.row()]->average = value.toDouble();
        break;
    }
    emit dataChanged(index, index);
    return true;
}

bool TabListModel::canFetchMore(const QModelIndex &) const
{
    return true;
}

void TabListModel::fetchMore(const QModelIndex &)
{
    emit lazyFetch();
}

void TabListModel::updateList(const QList<Video *> pl)
{
    rowList = pl;
    emit layoutChanged();
}
