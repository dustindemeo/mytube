#ifndef MYTUBE_H
#define MYTUBE_H

#include <QMainWindow>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtWebKit>
#include <QTableView>
#include <QUrl>
#include <QFile>
#include <QPoint>
#include <QMenu>
#include "ui_mytube.h"
#include "bottomlistmodel.h"
#include "currentlist.h"
#include "tablistmodel.h"
#include "searchlist.h"
#include "embed.h"
#include "thumbnaildelegate.h"
#include "about.h"

namespace Ui {
    class MyTube;
}

class MyTube : public QMainWindow
{
    Q_OBJECT

public:
    explicit MyTube(QWidget *parent = 0);
    ~MyTube();

public slots:
    void loadEmbed();

private slots:
    void on_actionQuit_2_triggered();
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionHelp_triggered();
    void on_search_returnPressed();
    void on_searchButton_clicked();
    void showCurrentTableContextMenu (const QPoint& pos);
    void showCurrentBottomTableContextMenu (const QPoint& pos);

private:
    Ui::MyTube *ui;
    SearchList *pl;
    CurrentList *cl;
};

#endif // MYTUBE_H
