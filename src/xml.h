#ifndef XML_H
#define XML_H

#define YT_XML_ENTRY "entry"
#define YT_XML_FEED "feed"

#define YT_XML_MEDIA "media"
#define YT_XML_CREDIT "credit"
#define YT_XML_TITLE "title"
#define YT_XML_CATEGORY "category"
#define YT_XML_DESCRIPTION "description"
#define YT_XML_THUMBNAIL "thumbnail"
#define YT_XML_A_NAME "yt:name"
#define YT_XML_A_HQDEFAULT "hqdefault"
#define YT_XML_A_URL "url"

#define YT_XML_YT "yt"
#define YT_XML_DURATION "duration"
#define YT_XML_A_SECONDS "seconds"
#define YT_XML_UPLOADED "uploaded"
#define YT_XML_VIDEOID "videoid"
#define YT_XML_STATISTICS "statistics"
#define YT_XML_A_FAVORITECOUNT "favoriteCount"
#define YT_XML_A_VIEWCOUNT "viewCount"
#define YT_XML_RATING "rating"
#define YT_XML_A_NUMLIKES "numLikes"
#define YT_XML_A_NUMDISLIKES "numDislikes"

#define YT_XML_GD "gd"
#define YT_XML_COMMENTS "feedLink"
#define YT_XML_A_HREF "href"
//#define YT_XML_RATING "rating"
#define YT_XML_A_AVERAGE "average"
#define YT_XML_A_NUMRATERS "numRaters"

#define MT_PLAYLIST "Playlist"
#define MT_VIDEO "Video"
#define MT_CREDIT "credit"
#define MT_TITLE "title"
#define MT_CATEG "categ"
#define MT_DESCRIPTION "description"
#define MT_THUMBNAIL "thumbnail"
#define MT_DURATION "duration"
#define MT_UPLOADED "uploaded"
#define MT_VIDEOID "videoid"
#define MT_FAVORITE_COUNT "favoriteCount"
#define MT_VIEW_COUNT "viewCount"
#define MT_NUM_LIKES "numLikes"
#define MT_NUM_DISLIKES "numDislikes"
#define MT_COMMENTS "comments"
#define MT_AVERAGE "average"
#define MT_NUM_RATERS "numRaters"




#endif // XML_H
