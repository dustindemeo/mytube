#include "video.h"

Video::Video(QObject *parent) : QObject(parent) {
}

void Video::xmlToVideo(QXmlStreamReader &xml) {
    QNetworkAccessManager *youtube;
    QNetworkRequest request;
    QNetworkReply *reply;
    QXmlStreamReader::TokenType type = xml.tokenType();
    QStringRef name = xml.name();
    QStringRef prefix = xml.prefix();
    QXmlStreamAttributes attributes = xml.attributes();
    if(!(type == QXmlStreamReader::StartElement && xml.name() == YT_XML_ENTRY))
        return;
    while(!(type == QXmlStreamReader::EndElement && name == YT_XML_ENTRY)) {
        if (type == QXmlStreamReader::StartElement) {
            if (prefix == YT_XML_MEDIA) {
                if (name == YT_XML_CREDIT)
                    credit = xml.readElementText();
                if (name == YT_XML_TITLE)
                    title = xml.readElementText();
                if (name == YT_XML_CATEGORY)
                    categ = xml.readElementText();
                if (name == YT_XML_DESCRIPTION)
                    description = xml.readElementText();
                if (name == YT_XML_THUMBNAIL) {
                    if (attributes.value(YT_XML_A_NAME) == YT_XML_A_HQDEFAULT) {
                        thumbnail = attributes.value(YT_XML_A_URL).toString();
                        youtube = new QNetworkAccessManager ();
                        request.setUrl (QUrl (thumbnail));
                        reply = youtube->get (request);
                        connect (reply, SIGNAL (finished ()), SLOT (saveThumbnail()));
                    }
                }
            }
            else if (prefix == YT_XML_YT) {
                if (name == YT_XML_DURATION)
                    duration = attributes.value(YT_XML_A_SECONDS).toString().toInt();
                if (name == YT_XML_UPLOADED)
                    uploaded = xml.readElementText();
                if (name == YT_XML_VIDEOID)
                    videoid = xml.readElementText();
                if (name == YT_XML_STATISTICS) {
                    favoriteCount = attributes.value(YT_XML_A_FAVORITECOUNT).toString().toInt();
                    viewCount = attributes.value(YT_XML_A_VIEWCOUNT).toString().toInt();
                }
                if (name == YT_XML_RATING) {
                    numLikes = attributes.value(YT_XML_A_NUMLIKES).toString().toInt();
                    numDislikes = attributes.value(YT_XML_A_NUMDISLIKES).toString().toInt();
                }
            }
            else if (prefix == YT_XML_GD) {
                if (name == YT_XML_COMMENTS)
                    comments = attributes.value(YT_XML_A_HREF).toString();
                if (name == YT_XML_RATING) {
                    average = attributes.value(YT_XML_A_AVERAGE).toString().toFloat();
                    numRaters = attributes.value(YT_XML_A_NUMRATERS).toString().toInt();
                }
            }
        }
        type = xml.readNext();
        name = xml.name();
        prefix = xml.prefix();
        attributes = xml.attributes();
    }
    xml.readNext();
}

void Video::playlistToVideo(QXmlStreamReader &xml) {
    QNetworkAccessManager *youtube;
    QNetworkRequest request;
    QNetworkReply *reply;
    QXmlStreamReader::TokenType type = xml.tokenType();
    QStringRef name = xml.name();
    if(!(type == QXmlStreamReader::StartElement && xml.name() == MT_VIDEO))
        return;
    while(!(type == QXmlStreamReader::EndElement && name == MT_VIDEO)) {
        if (name == MT_CREDIT) credit = xml.readElementText();
        if (name == MT_TITLE) title = xml.readElementText();
        if (name == MT_CATEG) categ = xml.readElementText();
        if (name == MT_DESCRIPTION) description = xml.readElementText();
        if (name == MT_THUMBNAIL) {
            thumbnail = xml.readElementText();
            youtube = new QNetworkAccessManager ();
            request.setUrl (QUrl (thumbnail));
            reply = youtube->get (request);
            connect (reply, SIGNAL (finished ()), SLOT (saveThumbnail()));
        }
        if (name == MT_DURATION) duration = xml.readElementText().toInt();
        if (name == MT_UPLOADED) uploaded = xml.readElementText();
        if (name == MT_VIDEOID) videoid = xml.readElementText();
        if (name == MT_FAVORITE_COUNT) favoriteCount = xml.readElementText().toInt();
        if (name == MT_VIEW_COUNT) viewCount = xml.readElementText().toInt();
        if (name == MT_NUM_LIKES) numLikes = xml.readElementText().toInt();
        if (name == MT_NUM_DISLIKES) numDislikes = xml.readElementText().toInt();
        if (name == MT_COMMENTS) comments = xml.readElementText();
        if (name == MT_AVERAGE) average = xml.readElementText().toFloat();
        if (name == MT_NUM_RATERS) numRaters = xml.readElementText().toInt();
        type = xml.readNext();
        name = xml.name();
    }
    xml.readNext();
    xml.readNext();
}

void Video::saveThumbnail()
{
    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error: " << reply->error();
        return;
    }
    QByteArray jpegData = reply->readAll();
    pixmap.loadFromData(jpegData);
    emit updatedPicture();
}
