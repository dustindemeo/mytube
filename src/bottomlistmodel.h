#ifndef CURRENTLISTMODEL_H
#define CURRENTLISTMODEL_H

#include <QAbstractTableModel>
#include <QVariant>
#include <QFile>
#include "embed.h"
#include "searchlist.h"
#include "video.h"

#define N_ROWS 3

class BottomListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit BottomListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);

signals:

public slots:
    void updateList(const QList<Video *> pl);

private:
    QList<Video *> colList;
};

#endif // CURRENTLISTMODEL_H
