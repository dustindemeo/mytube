#include <QApplication>
#include "mytube.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MyTube w;
    w.show();
    return app.exec();
}
