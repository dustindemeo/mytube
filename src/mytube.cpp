#include "mytube.h"

MyTube::MyTube(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MyTube)
{
    pl = new SearchList (parent);
    cl = new CurrentList (parent);
    ThumbnailDelegate *thumb = new ThumbnailDelegate();

    ui->setupUi(this);
    QNetworkProxyFactory::setUseSystemConfiguration(true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::AutoLoadImages, true);

    QFile file(EMBED_FILE);
    file.open(QFile::WriteOnly | QFile::Truncate);
    file.write(EMBED_HTML1_START);
    file.write("64vJoGcokVE");
    file.write(EMBED_HTML2_NOAUTO);
    file.close();

    ui->webView->load(QUrl(EMBED_FILE));
    ui->nowPlayingPic->setPixmap(QPixmap("logo.png").scaledToHeight(131));
    ui->nowPlayingCredit->setText("MyTube");
    ui->nowPlayingText->setText("by YouTwitFace");
    ui->tabs->addTab(ui->searchTable, "Search");
    ui->tabs->addTab(ui->currentTable, "Current");

    ui->toolbar->setStyleSheet("QToolBar { background-color: white; }");
    ui->toolbar->addSeparator();
    ui->toolbar->addWidget(ui->searchLabel);
    ui->searchLabel->setStyleSheet("QLabel { margin-left:20px; }");
    ui->toolbar->addWidget(ui->search);
    ui->search->setStyleSheet("QLineEdit { margin-left:20px; margin-right: 20px; }");
    ui->toolbar->addWidget(ui->searchButton);
    ui->searchButton->setStyleSheet("QPushButton { margin-right: 20px; width:100px; height:20px; }");

    BottomListModel *currentBottomModel = new BottomListModel (parent);
    ui->currentBottomTable->setModel(currentBottomModel);
    ui->currentBottomTable->setItemDelegateForRow(2, thumb);
    QObject::connect(ui->currentBottomTable, SIGNAL(doubleClicked(QModelIndex)),
                     cl, SLOT(updateBottomEmbed(QModelIndex)));
    ui->currentBottomTable->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->currentBottomTable, SIGNAL(customContextMenuRequested(const QPoint &)),
                     this, SLOT(showCurrentBottomTableContextMenu(const QPoint &)));
    QObject::connect(cl, SIGNAL(changedEmbed()),
                     this, SLOT(loadEmbed()));
    QObject::connect(cl, SIGNAL(changedNowPlayingPic(QPixmap)),
                     ui->nowPlayingPic, SLOT(setPixmap(QPixmap)));
    QObject::connect(cl, SIGNAL(changedNowPlayingCredit(QString)),
                     ui->nowPlayingCredit, SLOT(setText(QString)));
    QObject::connect(cl, SIGNAL(changedNowPlayingText(QString)),
                     ui->nowPlayingText, SLOT(setText(QString)));
    QObject::connect(cl, SIGNAL(changedCurrentList(QList<Video *>)),
                     currentBottomModel, SLOT(updateList(QList<Video *>)));

    TabListModel *currentTabModel = new TabListModel (parent);
    ui->currentTable->setModel(currentTabModel);
    ui->currentTable->setItemDelegateForColumn(0, thumb);
    QObject::connect(ui->currentTable, SIGNAL(doubleClicked(QModelIndex)),
                     cl, SLOT(updateTabEmbed(QModelIndex)));
    ui->currentTable->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->currentTable, SIGNAL(customContextMenuRequested(const QPoint &)),
                     this, SLOT(showCurrentTableContextMenu(const QPoint &)));
    QObject::connect(cl, SIGNAL(changedEmbed()),
                     this, SLOT(loadEmbed()));
    QObject::connect(cl, SIGNAL(changedNowPlayingPic(QPixmap)),
                     ui->nowPlayingPic, SLOT(setPixmap(QPixmap)));
    QObject::connect(cl, SIGNAL(changedNowPlayingCredit(QString)),
                     ui->nowPlayingCredit, SLOT(setText(QString)));
    QObject::connect(cl, SIGNAL(changedNowPlayingText(QString)),
                     ui->nowPlayingText, SLOT(setText(QString)));
    QObject::connect(cl, SIGNAL(changedCurrentList(QList<Video *>)),
                     currentTabModel, SLOT(updateList(QList<Video *>)));

    QObject::connect(ui->webView, SIGNAL(loadFinished(bool)),
                     cl, SLOT(queueNextEmbed(bool)));
    QObject::connect(currentTabModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                     ui->currentBottomTable, SLOT(reset()));
    QObject::connect(currentBottomModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                     ui->currentTable, SLOT(reset()));

    TabListModel *searchTabModel = new TabListModel (parent);
    ui->searchTable->setModel(searchTabModel);
    ui->searchTable->setItemDelegateForColumn(0, thumb);
    QObject::connect(ui->searchTable, SIGNAL(doubleClicked(QModelIndex)),
                     pl, SLOT(getSelectedVideo(QModelIndex)));
    QObject::connect(pl, SIGNAL (foundSelectedVideo(Video *)),
                     cl, SLOT (appendCurrentList(Video *)));
    QObject::connect(cl, SIGNAL(changedCurrentList(QList<Video *>)),
                     currentTabModel, SLOT(updateList(QList<Video*>)));
    QObject::connect(cl, SIGNAL(changedCurrentList(QList<Video *>)),
                     currentBottomModel, SLOT(updateList(QList<Video*>)));
    QObject::connect(pl, SIGNAL(changedSearchList(QList<Video *>)),
                     searchTabModel, SLOT(updateList(QList<Video *>)));
    QObject::connect(searchTabModel, SIGNAL(lazyFetch()),
                     pl, SLOT(appendSearchList()));
}

MyTube::~MyTube()
{
    delete ui;
}

void MyTube::loadEmbed()
{
    ui->webView->load(QUrl("http://www.stefanzh.com/projects/MyTube/server/embed.html"));
}

void MyTube::on_actionQuit_2_triggered()
{
    this->close();
}

void MyTube::on_actionNew_triggered()
{
    cl->clearPlaylist();
}

void MyTube::on_actionOpen_triggered()
{
    cl->loadPlaylist();
}

void MyTube::on_actionSave_triggered()
{
    cl->savePlaylist();
}

void MyTube::on_actionHelp_triggered()
{
    About a(this->parentWidget());
    a.exec();
}

void MyTube::on_search_returnPressed()
{
    pl->updateSearchList(ui->search->text());
    ui->tabs->setCurrentWidget(ui->searchTable);
}

void MyTube::on_searchButton_clicked()
{
    pl->updateSearchList(ui->search->text());
    ui->tabs->setCurrentWidget(ui->searchTable);
}

void MyTube::showCurrentTableContextMenu (const QPoint& pos)
{
    QPoint globalPos = ui->currentTable->viewport()->mapToGlobal(pos);
    QMenu myMenu;
    myMenu.addAction("Remove Video");
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem) {
        cl->removeVideoAt(ui->currentTable->indexAt(pos).row());
    }
}

void MyTube::showCurrentBottomTableContextMenu (const QPoint& pos)
{
    QPoint globalPos = ui->currentBottomTable->viewport()->mapToGlobal(pos);
    QMenu myMenu;
    myMenu.addAction("Remove Video");
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem) {
        cl->removeVideoAt(ui->currentBottomTable->indexAt(pos).column());
    }
}
