#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QXmlStreamReader>
#include <QList>
#include <QString>
#include "tablistmodel.h"
#include "video.h"
#include "xml.h"

class SearchList : public QObject {
    Q_OBJECT
public:
    explicit SearchList(QObject *parent = 0);
    void updateSearchList (QString str);

private:
    int startIndex;
    QString search;
    QList<Video *> videos;

signals:
    void changedSearchList(QList<Video *> pl);
    void foundSelectedVideo(Video *v);

public slots:
    void parseSearchXML ();
    void appendSearchList ();
    void updateSearchListPictures();
    void getSelectedVideo (QModelIndex index);

private slots:

};

#endif // PLAYLIST_H
