#ifndef EMBED_H
#define EMBED_H

#define EMBED_FILE "embed.html"

#define EMBED_HTML1_START "\
<html>\n\
<head>\n\
  <script src=\"http://www.stefanzh.com/projects/MyTube/server/jwplayer/jwplayer.js\" type=\"text/javascript\"></script>\n\
</head>\n\
<body style=\"width:520; height:380; background-color:#000000; margin:0px; padding:0px; \">\n\
<div id=\"container\" style=\"width=100%; height=100%; background-color:#000000; margin:0px; padding:0px; \">Loading the player ...</div>\n\
<script type=\"text/javascript\">\n\
 jwplayer(\"container\").setup({\n\
  flashplayer: \"http://www.stefanzh.com/projects/MyTube/server/jwplayer/player.swf\",\n\
  allowfullscreen: \"true\",\n\
  allowscriptaccess: \"true\",\n\
  bufferlength: \"1\",\n\
  stretching: \"uniform\",\n\
  bgcolor: \"#000000\",\n\
  wmode: \"opaque\",\n\
  height: 380,\n\
  width: 520,\n\
  events: { onComplete: function() { document.location = \"http://www.stefanzh.com/projects/MyTube/server/embed.html\"; } }, \n\
  file: \"file=http://www.youtube.com/watch?v="

#define EMBED_HTML2_NOAUTO "\",\n\
  autostart: \"false\"\n\
 });\n\
</script>\n\
</html>"

#define EMBED_HTML2_AUTO "\",\n\
  autostart: \"true\"\n\
 });\n\
</script>\n\
</html>"

#endif // EMBED_H
