#include "currentlist.h"

CurrentList::CurrentList(QObject *parent) :
    QObject(parent)
{
    currentIndex = 0;
    flagUpdateEmbed = false;
}

void CurrentList::removeVideoAt (int index)
{
    Video *v;
    if (index != -1) {
        v = videos.at(index);
        videos.removeAt(index);
        delete v;
        emit changedCurrentList(this->videos);
    }
}

void CurrentList::updateCurrentListPictures()
{
    emit changedCurrentList(this->videos);
}

void CurrentList::appendCurrentList (Video *v)
{
    videos.append(v);
    if ((currentIndex + 2) == videos.size()) {
        currentIndex ++;
        writeEmbed(true);
    }
    emit changedCurrentList(videos);
}

void CurrentList::updateBottomEmbed(QModelIndex index)
{
    currentIndex = index.column();
    flagUpdateEmbed = true;
    emit changedNowPlayingCredit (videos.at(currentIndex)->credit);
    emit changedNowPlayingText (videos.at(currentIndex)->title);
    emit changedNowPlayingPic(videos.at(currentIndex)->pixmap.scaledToWidth(171));
    writeEmbed(true);
}

void CurrentList::updateTabEmbed(QModelIndex index)
{
    currentIndex = index.row();
    flagUpdateEmbed = true;
    emit changedNowPlayingCredit (videos.at(currentIndex)->credit);
    emit changedNowPlayingText (videos.at(currentIndex)->title);
    emit changedNowPlayingPic(videos.at(currentIndex)->pixmap.scaledToWidth(171));
    writeEmbed(true);
}

void CurrentList::queueNextEmbed (bool b)
{
    if (((currentIndex + 1) < videos.size()) && b) {
        emit changedNowPlayingCredit (videos.at(currentIndex)->credit);
        emit changedNowPlayingText (videos.at(currentIndex)->title);
        emit changedNowPlayingPic(videos.at(currentIndex)->pixmap.scaledToWidth(171));
        currentIndex ++;
        writeEmbed(true);
    }
    else if ((videos.size() > 0) && b) {
        emit changedNowPlayingCredit (videos.at(currentIndex)->credit);
        emit changedNowPlayingText (videos.at(currentIndex)->title);
        emit changedNowPlayingPic(videos.at(currentIndex)->pixmap.scaledToWidth(171));
        writeEmbed(false);
    }
}

void CurrentList::writeEmbed(bool b)
{
    QByteArray data;
    QString body = EMBED_HTML1_START + videos.at(currentIndex)->videoid;
    if (b) body += EMBED_HTML2_AUTO;
    else body += EMBED_HTML2_NOAUTO;
    data = body.toLatin1();
    QNetworkAccessManager *cis190 = new QNetworkAccessManager(this);
    QNetworkRequest request(QUrl("http://www.stefanzh.com/projects/MyTube/server/embed.php"));
    request.setHeader(QNetworkRequest::ContentLengthHeader,data.size());
    QObject::connect(cis190, SIGNAL(finished(QNetworkReply *)), this, SLOT(embedReply()));
    cis190->post(request,data);
}

void CurrentList::embedReply()
{
    if (flagUpdateEmbed) {
        emit changedEmbed();
        flagUpdateEmbed = false;
    }
}

void CurrentList::savePlaylist ()
{
    Video *v;
    QFile file(SAVE_FILE);
    file.open(QFile::WriteOnly | QFile::Truncate);
    QXmlStreamWriter xmlWriter;
    xmlWriter.setDevice(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Playlist");
    for (int i=0; i<videos.size(); i++) {
        v = videos.at(i);
        xmlWriter.writeStartElement("Video");
        xmlWriter.writeTextElement(MT_CREDIT, v->credit);
        xmlWriter.writeTextElement(MT_TITLE, v->title);
        xmlWriter.writeTextElement(MT_CATEG, v->categ);
        xmlWriter.writeTextElement(MT_DESCRIPTION, v->description);
        xmlWriter.writeTextElement(MT_THUMBNAIL, v->thumbnail);
        xmlWriter.writeTextElement(MT_DURATION, QString::number(v->duration));
        xmlWriter.writeTextElement(MT_UPLOADED, v->uploaded);
        xmlWriter.writeTextElement(MT_VIDEOID, v->videoid);
        xmlWriter.writeTextElement(MT_FAVORITE_COUNT, QString::number(v->favoriteCount));
        xmlWriter.writeTextElement(MT_VIEW_COUNT, QString::number(v->viewCount));
        xmlWriter.writeTextElement(MT_NUM_LIKES, QString::number(v->numLikes));
        xmlWriter.writeTextElement(MT_NUM_DISLIKES, QString::number(v->numDislikes));
        xmlWriter.writeTextElement(MT_COMMENTS, v->comments);
        xmlWriter.writeTextElement(MT_AVERAGE, QString::number(v->average));
        xmlWriter.writeTextElement(MT_NUM_RATERS, QString::number(v->numRaters));
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();
}

void CurrentList::loadPlaylist ()
{
    qDeleteAll(videos.begin(), videos.end());
    videos.clear();
    Video *v;
    QFile file(SAVE_FILE);
    file.open(QFile::ReadOnly);
    QXmlStreamReader xml;
    xml.setDevice(&file);
    while (!xml.atEnd() && !xml.hasError() && xml.name() != MT_VIDEO)
    {
        xml.readNext();
    }
    while (!xml.atEnd() && !xml.hasError() && xml.name() != MT_PLAYLIST) {
        v = new Video();
        QObject::connect(v, SIGNAL(updatedPicture()),
                         this, SLOT (updateCurrentListPictures()));
        v->playlistToVideo(xml);
        videos.append (v);
    }
    if (xml.hasError())
    {
        qDebug() << "Error: " << xml.errorString();
    }
    file.close();
    emit changedCurrentList(videos);
}

void CurrentList::clearPlaylist ()
{
    qDeleteAll(videos.begin(), videos.end());
    videos.clear();
    QFile file(SAVE_FILE);
    file.open(QFile::WriteOnly | QFile::Truncate);
    file.close();
    emit changedCurrentList(videos);
}
