#ifndef CURRENTLIST_H
#define CURRENTLIST_H

#include <QObject>
#include <QList>
#include "bottomlistmodel.h"
#include "video.h"
#include "xml.h"

#define SAVE_FILE "save.xml"

class CurrentList : public QObject
{
    Q_OBJECT
public:
    explicit CurrentList(QObject *parent = 0);
    void savePlaylist ();
    void loadPlaylist ();
    void clearPlaylist ();
    void removeVideoAt (int index);

private:
    int currentIndex;
    bool flagUpdateEmbed;
    QList<Video *> videos;

    void writeEmbed(bool b);

signals:
    void changedCurrentList(QList<Video *> pl);
    void changedEmbed();
    void changedNowPlayingCredit (QString s);
    void changedNowPlayingText (QString s);
    void changedNowPlayingPic (QPixmap p);

public slots:
    void updateBottomEmbed(QModelIndex index);
    void updateTabEmbed(QModelIndex index);
    void queueNextEmbed (bool b);
    void appendCurrentList (Video *v);
    void updateCurrentListPictures();
    void embedReply();


signals:

public slots:

};

#endif // CURRENTLIST_H
