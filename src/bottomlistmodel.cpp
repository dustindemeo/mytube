#include "bottomlistmodel.h"

BottomListModel::BottomListModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int BottomListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return N_ROWS;
}

int BottomListModel::columnCount(const QModelIndex &/*parent*/) const
{
    return colList.size();
}

QVariant BottomListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::TextAlignmentRole) {
            return int(Qt::AlignLeft | Qt::AlignTop);
    }
    else if (role == Qt::DisplayRole) {
        switch (index.row()) {
        case 0:
            return colList[index.column()]->credit;
            break;
        case 1:
            return colList[index.column()]->title;
            break;
        case 2:
            return colList[index.column()]->pixmap;
            break;
        }
    }
    return QVariant();
}

Qt::ItemFlags BottomListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    if (index.row() == 2)
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    else
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

bool BottomListModel::setData(const QModelIndex &index,
                            const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    switch (index.row()) {
    case 0:
        colList[index.column()]->credit = value.toString();
        break;
    case 1:
        colList[index.column()]->title = value.toString();
        break;
    }
    emit dataChanged(index, index);
    return true;
}

void BottomListModel::updateList(const QList<Video *> pl)
{
    colList = pl;
    emit layoutChanged();
}
