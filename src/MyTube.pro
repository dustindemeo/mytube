#-------------------------------------------------
#
# Project created by QtCreator 2011-12-03T16:06:03
#
#-------------------------------------------------

QT       += core gui network webkitwidgets declarative

TARGET = MyTube
TEMPLATE = app

SOURCES +=  main.cpp\
            video.cpp \
            mytube.cpp \
    thumbnaildelegate.cpp \
    searchlist.cpp \
    currentlist.cpp \
    tablistmodel.cpp \
    bottomlistmodel.cpp \
    about.cpp

HEADERS  += video.h \
            xml.h \
            mytube.h \
    embed.h \
    thumbnaildelegate.h \
    searchlist.h \
    currentlist.h \
    tablistmodel.h \
    bottomlistmodel.h \
    about.h

RESOURCES += icons.qrc

FORMS    += mytube.ui \
            about.ui

OTHER_FILES +=
