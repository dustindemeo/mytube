#include "about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->icon->setPixmap(QPixmap("logo.png").scaledToHeight(131));
    ui->about->setText(ABOUT_TEXT);
}

void About::on_pushButton_clicked()
{
    this->close();
}
