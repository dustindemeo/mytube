#ifndef ABOUT_H
#define ABOUT_H

#include <QMainWindow>
#include "ui_about.h"

#define ABOUT_TEXT "MyTube v0.1\n\nThank you from Team YouTwitFace.\n\nBen\nDustin\nStefan\nYing"

namespace Ui {
    class About;
}

class About : public QDialog
{
    Q_OBJECT
public:
    explicit About(QWidget *parent = 0);

private:
    Ui::About *ui;

signals:

public slots:

private slots:
    void on_pushButton_clicked();
};

#endif // ABOUT_H
