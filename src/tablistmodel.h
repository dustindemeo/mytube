#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractTableModel>
#include <QVariant>
#include <QFile>
#include <QTime>
#include "embed.h"
#include "searchlist.h"
#include "video.h"

#define N_COLUMNS 6

class TabListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TabListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);

signals:
    void lazyFetch();

protected:
    bool canFetchMore(const QModelIndex &parent) const;
    void fetchMore(const QModelIndex &parent);

public slots:
    void updateList(const QList<Video *> pl);

private:
    QList<Video *> rowList;

};

#endif // PLAYLISTMODEL_H
