#ifndef VIDEO_H
#define VIDEO_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QXmlStreamReader>
#include <QPixmap>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include "xml.h"

class Video : public QObject {
    Q_OBJECT
public:
    explicit Video(QObject *parent = 0);

    void xmlToVideo(QXmlStreamReader &xml);
    void playlistToVideo(QXmlStreamReader &xml);
    void setEmbed();

    QString credit;
    QString title;
    QString categ;
    QString description;
    QString thumbnail;

    int duration;
    QString uploaded;
    QString videoid;
    int favoriteCount;
    int viewCount;
    int numLikes;
    int numDislikes;

    QString comments;
    float average;
    int numRaters;

    QPixmap pixmap;

private:

signals:
    void updatedPicture();

private slots:
    void saveThumbnail();

};

#endif // VIDEO_H
