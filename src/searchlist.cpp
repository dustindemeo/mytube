#include "searchlist.h"

SearchList::SearchList(QObject *parent) : QObject(parent) {
    QNetworkAccessManager *youtube = new QNetworkAccessManager (this);
    QNetworkRequest request;
    request.setUrl (QUrl ("https://gdata.youtube.com/feeds/api/standardfeeds/top_rated?v=2"));
    QNetworkReply *reply = youtube->get (request);
    connect (reply, SIGNAL (finished ()), SLOT (parseSearchXML()));
}

void SearchList::updateSearchListPictures()
{
    emit changedSearchList(this->videos);
}

void SearchList::updateSearchList (QString str)
{
    QString url;
    search = str;
    search.replace(" ", "+");
    startIndex = 1;
    qDeleteAll(this->videos.begin(), this->videos.end());
    this->videos.clear();
    url = "https://gdata.youtube.com/feeds/api/videos?max-results=10&start-index=";
    url.append(QString("%1").arg(startIndex));
    url.append("&v=2&q=");
    url.append(search);
    startIndex += 10;
    QNetworkAccessManager *youtube = new QNetworkAccessManager (this);
    QNetworkRequest request;
    request.setUrl (QUrl (url));
    QNetworkReply *reply = youtube->get (request);
    connect (reply, SIGNAL (finished ()), SLOT (parseSearchXML()));
}

void SearchList::appendSearchList ()
{
    startIndex += 10;
    QString url;
    url = "https://gdata.youtube.com/feeds/api/videos?max-results=10&start-index=";
    url.append(QString("%1").arg(startIndex));
    url.append("&v=2&q=");
    url.append(search);
    QNetworkAccessManager *youtube = new QNetworkAccessManager (this);
    QNetworkRequest request;
    request.setUrl (QUrl (url));
    QNetworkReply *reply = youtube->get (request);
    connect (reply, SIGNAL (finished ()), SLOT (parseSearchXML()));
}

void SearchList::parseSearchXML () {
    Video *v;
    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error: " << reply->error();
        return;
    }
    QXmlStreamReader xml (reply->readAll());
    while (!xml.atEnd() && !xml.hasError() && xml.name() != YT_XML_ENTRY)
    {
        xml.readNext();
    }
    while (!xml.atEnd() && !xml.hasError() && xml.name() != YT_XML_FEED) {
        v = new Video();
        QObject::connect(v, SIGNAL(updatedPicture()),
                         this, SLOT (updateSearchListPictures()));
        v->xmlToVideo(xml);
        videos.append (v);
    }
    if (xml.hasError())
    {
        qDebug() << "Error: " << xml.errorString();
    }
    xml.clear();
    reply->deleteLater();
    emit changedSearchList(videos);
}

void SearchList::getSelectedVideo (QModelIndex index)
{
    emit foundSelectedVideo(videos.at(index.row()));
    videos.removeAt(index.row());
    emit changedSearchList(videos);
}
