#ifndef THUMBNAILDELEGATE_H
#define THUMBNAILDELEGATE_H

#include <QStyledItemDelegate>
#include <QPixmap>
#include <QPainter>

class ThumbnailDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit ThumbnailDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const;

private:

signals:

public slots:

};

#endif // THUMBNAILDELEGATE_H
