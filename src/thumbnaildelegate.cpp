#include "thumbnaildelegate.h"

ThumbnailDelegate::ThumbnailDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

void ThumbnailDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
    painter->save();
    QPixmap p = index.data().value<QPixmap>();
    painter->drawPixmap(option.rect.adjusted(5,2,-5,-2), p);
    painter->restore();
}
