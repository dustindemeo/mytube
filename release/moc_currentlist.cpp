/****************************************************************************
** Meta object code from reading C++ file 'currentlist.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.0.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/currentlist.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'currentlist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CurrentList_t {
    QByteArrayData data[21];
    char stringdata[254];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_CurrentList_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_CurrentList_t qt_meta_stringdata_CurrentList = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 18),
QT_MOC_LITERAL(2, 31, 0),
QT_MOC_LITERAL(3, 32, 13),
QT_MOC_LITERAL(4, 46, 2),
QT_MOC_LITERAL(5, 49, 12),
QT_MOC_LITERAL(6, 62, 23),
QT_MOC_LITERAL(7, 86, 1),
QT_MOC_LITERAL(8, 88, 21),
QT_MOC_LITERAL(9, 110, 20),
QT_MOC_LITERAL(10, 131, 1),
QT_MOC_LITERAL(11, 133, 17),
QT_MOC_LITERAL(12, 151, 5),
QT_MOC_LITERAL(13, 157, 14),
QT_MOC_LITERAL(14, 172, 14),
QT_MOC_LITERAL(15, 187, 1),
QT_MOC_LITERAL(16, 189, 17),
QT_MOC_LITERAL(17, 207, 6),
QT_MOC_LITERAL(18, 214, 1),
QT_MOC_LITERAL(19, 216, 25),
QT_MOC_LITERAL(20, 242, 10)
    },
    "CurrentList\0changedCurrentList\0\0"
    "QList<Video*>\0pl\0changedEmbed\0"
    "changedNowPlayingCredit\0s\0"
    "changedNowPlayingText\0changedNowPlayingPic\0"
    "p\0updateBottomEmbed\0index\0updateTabEmbed\0"
    "queueNextEmbed\0b\0appendCurrentList\0"
    "Video*\0v\0updateCurrentListPictures\0"
    "embedReply\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CurrentList[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x05,
       5,    0,   72,    2, 0x05,
       6,    1,   73,    2, 0x05,
       8,    1,   76,    2, 0x05,
       9,    1,   79,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      11,    1,   82,    2, 0x0a,
      13,    1,   85,    2, 0x0a,
      14,    1,   88,    2, 0x0a,
      16,    1,   91,    2, 0x0a,
      19,    0,   94,    2, 0x0a,
      20,    0,   95,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QPixmap,   10,

 // slots: parameters
    QMetaType::Void, QMetaType::QModelIndex,   12,
    QMetaType::Void, QMetaType::QModelIndex,   12,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CurrentList::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CurrentList *_t = static_cast<CurrentList *>(_o);
        switch (_id) {
        case 0: _t->changedCurrentList((*reinterpret_cast< QList<Video*>(*)>(_a[1]))); break;
        case 1: _t->changedEmbed(); break;
        case 2: _t->changedNowPlayingCredit((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->changedNowPlayingText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->changedNowPlayingPic((*reinterpret_cast< QPixmap(*)>(_a[1]))); break;
        case 5: _t->updateBottomEmbed((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 6: _t->updateTabEmbed((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 7: _t->queueNextEmbed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->appendCurrentList((*reinterpret_cast< Video*(*)>(_a[1]))); break;
        case 9: _t->updateCurrentListPictures(); break;
        case 10: _t->embedReply(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<Video*> >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Video* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CurrentList::*_t)(QList<Video*> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CurrentList::changedCurrentList)) {
                *result = 0;
            }
        }
        {
            typedef void (CurrentList::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CurrentList::changedEmbed)) {
                *result = 1;
            }
        }
        {
            typedef void (CurrentList::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CurrentList::changedNowPlayingCredit)) {
                *result = 2;
            }
        }
        {
            typedef void (CurrentList::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CurrentList::changedNowPlayingText)) {
                *result = 3;
            }
        }
        {
            typedef void (CurrentList::*_t)(QPixmap );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CurrentList::changedNowPlayingPic)) {
                *result = 4;
            }
        }
    }
}

const QMetaObject CurrentList::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CurrentList.data,
      qt_meta_data_CurrentList,  qt_static_metacall, 0, 0}
};


const QMetaObject *CurrentList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CurrentList::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CurrentList.stringdata))
        return static_cast<void*>(const_cast< CurrentList*>(this));
    return QObject::qt_metacast(_clname);
}

int CurrentList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void CurrentList::changedCurrentList(QList<Video*> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CurrentList::changedEmbed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void CurrentList::changedNowPlayingCredit(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CurrentList::changedNowPlayingText(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void CurrentList::changedNowPlayingPic(QPixmap _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
