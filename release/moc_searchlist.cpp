/****************************************************************************
** Meta object code from reading C++ file 'searchlist.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.0.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/searchlist.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'searchlist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SearchList_t {
    QByteArrayData data[13];
    char stringdata[156];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_SearchList_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_SearchList_t qt_meta_stringdata_SearchList = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 17),
QT_MOC_LITERAL(2, 29, 0),
QT_MOC_LITERAL(3, 30, 13),
QT_MOC_LITERAL(4, 44, 2),
QT_MOC_LITERAL(5, 47, 18),
QT_MOC_LITERAL(6, 66, 6),
QT_MOC_LITERAL(7, 73, 1),
QT_MOC_LITERAL(8, 75, 14),
QT_MOC_LITERAL(9, 90, 16),
QT_MOC_LITERAL(10, 107, 24),
QT_MOC_LITERAL(11, 132, 16),
QT_MOC_LITERAL(12, 149, 5)
    },
    "SearchList\0changedSearchList\0\0"
    "QList<Video*>\0pl\0foundSelectedVideo\0"
    "Video*\0v\0parseSearchXML\0appendSearchList\0"
    "updateSearchListPictures\0getSelectedVideo\0"
    "index\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SearchList[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x05,
       5,    1,   47,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       8,    0,   50,    2, 0x0a,
       9,    0,   51,    2, 0x0a,
      10,    0,   52,    2, 0x0a,
      11,    1,   53,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   12,

       0        // eod
};

void SearchList::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SearchList *_t = static_cast<SearchList *>(_o);
        switch (_id) {
        case 0: _t->changedSearchList((*reinterpret_cast< QList<Video*>(*)>(_a[1]))); break;
        case 1: _t->foundSelectedVideo((*reinterpret_cast< Video*(*)>(_a[1]))); break;
        case 2: _t->parseSearchXML(); break;
        case 3: _t->appendSearchList(); break;
        case 4: _t->updateSearchListPictures(); break;
        case 5: _t->getSelectedVideo((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<Video*> >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Video* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SearchList::*_t)(QList<Video*> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SearchList::changedSearchList)) {
                *result = 0;
            }
        }
        {
            typedef void (SearchList::*_t)(Video * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SearchList::foundSelectedVideo)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject SearchList::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SearchList.data,
      qt_meta_data_SearchList,  qt_static_metacall, 0, 0}
};


const QMetaObject *SearchList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SearchList::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SearchList.stringdata))
        return static_cast<void*>(const_cast< SearchList*>(this));
    return QObject::qt_metacast(_clname);
}

int SearchList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void SearchList::changedSearchList(QList<Video*> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SearchList::foundSelectedVideo(Video * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
