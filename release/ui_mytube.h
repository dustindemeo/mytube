/********************************************************************************
** Form generated from reading UI file 'mytube.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYTUBE_H
#define UI_MYTUBE_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyTube
{
public:
    QAction *actionSave;
    QAction *actionOpen;
    QAction *actionQuit_2;
    QAction *actionNew;
    QAction *actionHelp;
    QWidget *centralWidget;
    QWebView *webView;
    QLineEdit *search;
    QLabel *searchLabel;
    QPushButton *searchButton;
    QTableView *searchTable;
    QTabWidget *tabs;
    QLabel *nowPlayingPic;
    QLabel *nowPlayingText;
    QTableView *currentBottomTable;
    QTableView *currentTable;
    QLabel *nowPlayingCredit;
    QToolBar *toolbar;

    void setupUi(QMainWindow *MyTube)
    {
        if (MyTube->objectName().isEmpty())
            MyTube->setObjectName(QStringLiteral("MyTube"));
        MyTube->resize(1015, 600);
        QIcon icon;
        icon.addFile(QStringLiteral("windowicon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MyTube->setWindowIcon(icon);
        actionSave = new QAction(MyTube);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        QIcon icon1;
        icon1.addFile(QStringLiteral("save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon1);
        actionOpen = new QAction(MyTube);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        QIcon icon2;
        icon2.addFile(QStringLiteral("openlist.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon2);
        actionQuit_2 = new QAction(MyTube);
        actionQuit_2->setObjectName(QStringLiteral("actionQuit_2"));
        QIcon icon3;
        icon3.addFile(QStringLiteral("exitlist.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionQuit_2->setIcon(icon3);
        actionNew = new QAction(MyTube);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        QIcon icon4;
        icon4.addFile(QStringLiteral("new.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew->setIcon(icon4);
        actionHelp = new QAction(MyTube);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        QIcon icon5;
        icon5.addFile(QStringLiteral("help.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelp->setIcon(icon5);
        centralWidget = new QWidget(MyTube);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        webView = new QWebView(centralWidget);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setGeometry(QRect(5, 0, 520, 380));
        webView->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        webView->setUrl(QUrl(QStringLiteral("about:blank")));
        search = new QLineEdit(centralWidget);
        search->setObjectName(QStringLiteral("search"));
        search->setGeometry(QRect(90, 110, 211, 27));
        searchLabel = new QLabel(centralWidget);
        searchLabel->setObjectName(QStringLiteral("searchLabel"));
        searchLabel->setGeometry(QRect(10, 120, 67, 17));
        searchButton = new QPushButton(centralWidget);
        searchButton->setObjectName(QStringLiteral("searchButton"));
        searchButton->setGeometry(QRect(310, 110, 98, 27));
        searchTable = new QTableView(centralWidget);
        searchTable->setObjectName(QStringLiteral("searchTable"));
        searchTable->setGeometry(QRect(530, 0, 481, 391));
        searchTable->setStyleSheet(QStringLiteral("background-image: url(:/new.jpg);"));
        searchTable->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        searchTable->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        searchTable->setShowGrid(false);
        searchTable->setSortingEnabled(true);
        searchTable->horizontalHeader()->setVisible(true);
        searchTable->horizontalHeader()->setDefaultSectionSize(110);
        searchTable->horizontalHeader()->setMinimumSectionSize(50);
        searchTable->verticalHeader()->setVisible(false);
        searchTable->verticalHeader()->setDefaultSectionSize(60);
        searchTable->verticalHeader()->setHighlightSections(false);
        searchTable->verticalHeader()->setMinimumSectionSize(60);
        tabs = new QTabWidget(centralWidget);
        tabs->setObjectName(QStringLiteral("tabs"));
        tabs->setGeometry(QRect(530, 0, 481, 381));
        tabs->setStyleSheet(QStringLiteral("background-image: url(:/new.jpg);"));
        nowPlayingPic = new QLabel(centralWidget);
        nowPlayingPic->setObjectName(QStringLiteral("nowPlayingPic"));
        nowPlayingPic->setGeometry(QRect(5, 420, 171, 131));
        nowPlayingText = new QLabel(centralWidget);
        nowPlayingText->setObjectName(QStringLiteral("nowPlayingText"));
        nowPlayingText->setGeometry(QRect(5, 400, 171, 21));
        nowPlayingText->setScaledContents(true);
        nowPlayingText->setWordWrap(true);
        currentBottomTable = new QTableView(centralWidget);
        currentBottomTable->setObjectName(QStringLiteral("currentBottomTable"));
        currentBottomTable->setGeometry(QRect(190, 384, 821, 171));
        currentBottomTable->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        currentBottomTable->setProperty("showDropIndicator", QVariant(true));
        currentBottomTable->setDragDropOverwriteMode(true);
        currentBottomTable->setShowGrid(false);
        currentBottomTable->horizontalHeader()->setVisible(false);
        currentBottomTable->horizontalHeader()->setDefaultSectionSize(150);
        currentBottomTable->horizontalHeader()->setMinimumSectionSize(150);
        currentBottomTable->verticalHeader()->setVisible(false);
        currentBottomTable->verticalHeader()->setDefaultSectionSize(20);
        currentBottomTable->verticalHeader()->setMinimumSectionSize(20);
        currentBottomTable->verticalHeader()->setStretchLastSection(true);
        currentTable = new QTableView(centralWidget);
        currentTable->setObjectName(QStringLiteral("currentTable"));
        currentTable->setGeometry(QRect(530, 0, 481, 391));
        currentTable->setStyleSheet(QStringLiteral("background-image: url(:/new.jpg);"));
        currentTable->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        currentTable->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        currentTable->setShowGrid(false);
        currentTable->setSortingEnabled(true);
        currentTable->horizontalHeader()->setVisible(true);
        currentTable->horizontalHeader()->setDefaultSectionSize(110);
        currentTable->horizontalHeader()->setMinimumSectionSize(50);
        currentTable->verticalHeader()->setVisible(false);
        currentTable->verticalHeader()->setDefaultSectionSize(60);
        currentTable->verticalHeader()->setHighlightSections(false);
        currentTable->verticalHeader()->setMinimumSectionSize(60);
        nowPlayingCredit = new QLabel(centralWidget);
        nowPlayingCredit->setObjectName(QStringLiteral("nowPlayingCredit"));
        nowPlayingCredit->setGeometry(QRect(5, 380, 171, 21));
        nowPlayingCredit->setScaledContents(true);
        nowPlayingCredit->setWordWrap(true);
        MyTube->setCentralWidget(centralWidget);
        tabs->raise();
        searchTable->raise();
        webView->raise();
        search->raise();
        searchLabel->raise();
        searchButton->raise();
        nowPlayingPic->raise();
        nowPlayingText->raise();
        currentBottomTable->raise();
        currentTable->raise();
        nowPlayingCredit->raise();
        toolbar = new QToolBar(MyTube);
        toolbar->setObjectName(QStringLiteral("toolbar"));
        MyTube->addToolBar(Qt::TopToolBarArea, toolbar);

        toolbar->addAction(actionNew);
        toolbar->addAction(actionOpen);
        toolbar->addAction(actionSave);
        toolbar->addSeparator();
        toolbar->addAction(actionHelp);
        toolbar->addAction(actionQuit_2);
        toolbar->addSeparator();

        retranslateUi(MyTube);

        QMetaObject::connectSlotsByName(MyTube);
    } // setupUi

    void retranslateUi(QMainWindow *MyTube)
    {
        MyTube->setWindowTitle(QApplication::translate("MyTube", "MyTube", 0));
        actionSave->setText(QApplication::translate("MyTube", "Save", 0));
#ifndef QT_NO_TOOLTIP
        actionSave->setToolTip(QApplication::translate("MyTube", "Save", 0));
#endif // QT_NO_TOOLTIP
        actionOpen->setText(QApplication::translate("MyTube", "Open", 0));
        actionQuit_2->setText(QApplication::translate("MyTube", "Quit", 0));
        actionQuit_2->setShortcut(QApplication::translate("MyTube", "Ctrl+Q", 0));
        actionNew->setText(QApplication::translate("MyTube", "New", 0));
        actionHelp->setText(QApplication::translate("MyTube", "help", 0));
        searchLabel->setText(QApplication::translate("MyTube", "Search YouTube", 0));
        searchButton->setText(QApplication::translate("MyTube", "Search", 0));
        nowPlayingPic->setText(QApplication::translate("MyTube", "TextLabel", 0));
        nowPlayingText->setText(QApplication::translate("MyTube", "TextLabel", 0));
        nowPlayingCredit->setText(QApplication::translate("MyTube", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class MyTube: public Ui_MyTube {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYTUBE_H
